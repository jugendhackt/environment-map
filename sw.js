//This array should NEVER contain any file which doesn't exist. Otherwise no single file can be cached.
var preCache=[
  '/index.html',
  '/',
  '/main.js',
  '/styles.css',
  '/api.js',
  '/data/eco_problems.json',
  '/alpaka.png',
  '/manifest.json',
  '/Alpaka.mp3',
  '/robots.txt',
  '/sw.js',
  '/leaflet.js',
  '/leaflet.css',
  '/images/icons/icon-128x128.png',
'/images/icons/icon-144x144.png',
'/images/icons/icon-152x152.png',
'/images/icons/icon-192x192.png',
'/images/icons/icon-384x384.png',
'/images/icons/icon-512x512.png',
'/images/icons/icon-72x72.png',
'/images/icons/icon-96x96.png',
'/images/layers-2x.png',
'/images/layers.png',
'/images/marker-icon-2x.png',
'/images/marker-icon.png',
'/images/marker-shadow.png',
];
//Please specify the version off your App. For every new version, any files are being refreched.
var appVersion="v1";
//Please specify all files which sould never be cached
var noCache=[
];

//On installation of app, all files from preCache are being stored automatically.
self.addEventListener('install', function(event) {
  event.waitUntil(
      caches.open(appVersion+'-offline').then(function(cache) {
          return cache.addAll(preCache).then(function(){
            console.log('mtSW: Given files were successfully pre-cached')
          });
      })
  );
});

//If any fetch fails, it will look for the request in the cache and serve it from there first
self.addEventListener('fetch', function(event) {
  //Trying to answer with "online" version if fails, using cache.
  event.respondWith(
    fetch(event.request).then(function (response) {
      //Checking if url is market as noCache
      var isNoCache=noCache.includes(response.url.substr(8).substr(response.url.substr(8).indexOf("/")));
      //Checking of hostname of request != current hostname
      var isOtherHost=response.url.substr(8).substr(0,response.url.substr(8).indexOf("/"))!=location.hostname&&response.url.substr(7).substr(0,response.url.substr(7).indexOf("/"))!=location.hostname;
      if((response.url.substr(0,4)=="http"||response.url.substr(0,3)=="ftp") && isNoCache==false && isOtherHost==false) {
        console.log('mtSW: Added file to cache: '+response.url);
        cache.put(response.url, response);
      } else {
        console.log('mtSW: The url scheme of '+response.url+' is not supported by cache or manually marked as noCache. Skipping.');
      }
	return(response);
    }).catch(function(error) {
      console.log( 'mtSW: Offline. Serving content from cache: ' + error );

      //Check to see if you have it in the cache
      //Return response
      //If not in the cache, then return error page
      return caches.open(appVersion+'-offline').then(function (cache) {
        return cache.match(event.request).then(function (matching) {
          var report =  !matching || matching.status == 404?Promise.reject('no-match'): matching;
          return report
        });
      });
    })
  );
})
